package com.shapravskyi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "caloric_content",
        "type",
        "ingredients",
        "values",
        "production"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Candy {
    private String name;
    private int caloricContent;
    private String type;
    private Ingredient ingredients;
    private NutritionalValue values;
    private String production;

    /**
     * No args constructor for use in serialization
     *
     */
    public Candy() {
    }

    /**
     *
     * @param ingredients
     * @param values
     * @param name
     * @param caloricContent
     * @param type
     * @param production
     */
    public Candy(String name, int caloricContent, String type, Ingredient ingredients,
                 NutritionalValue values, String production) {
        this.name = name;
        this.caloricContent = caloricContent;
        this.type = type;
        this.ingredients = ingredients;
        this.values = values;
        this.production = production;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCaloricContent() {
        return caloricContent;
    }

    @JsonSetter("caloric_content")
    public void setCaloricContent(int caloricContent) {
        this.caloricContent = caloricContent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public NutritionalValue getValues() {
        return values;
    }

    public void setValues(NutritionalValue values) {
        this.values = values;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public Ingredient getIngredient() {
        return ingredients;
    }

    public void setIngredient(Ingredient ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "Name: " + name + ", " + "Caloric content: " + caloricContent + ", " +
                "Type: " + type + ", " + "Ingredients: " + ingredients.toString() + ", " +
                "Nutritional values: " + values.toString() + ", " + "Production: " + production;
    }
}
