package com.shapravskyi.model;

import com.fasterxml.jackson.annotation.*;

import java.util.List;

@JsonRootName(value = "sweets")
public class SweetsRoot {
    private List<Object> sweets = null;

    public SweetsRoot() {

    }

    public SweetsRoot(List<Object> sweets) {
        this.sweets = sweets;
    }

    @JsonProperty("sweets")
    public List<Object> getSweets() {
        return sweets;
    }

    @JsonProperty("sweets")
    public void setSweets(List<Object> sweets) {
        this.sweets = sweets;
    }

    @Override
    public String toString() {
        for(Object candy: sweets) {
            return sweets.toString();
        }
        return " ";
    }
}
