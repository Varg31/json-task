package com.shapravskyi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "proteins",
        "fats",
        "carbohydrates"
})
public class NutritionalValue {

    private double proteins;
    private double fats;
    private double carbohydrates;

    /**
     * No args constructor for use in serialization
     *
     */
    public NutritionalValue() {
    }

    /**
     *
     * @param fats
     * @param carbohydrates
     * @param proteins
     */
    public NutritionalValue(double proteins, double fats, double carbohydrates) {
        this.proteins = proteins;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public double getFats() {
        return fats;
    }

    public void setFats(double fats) {
        this.fats = fats;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "Proteins: " + proteins + "g, " + "Fats: " + fats + "g, " +
                "Carbohydrates: " + carbohydrates + "g";
    }

}