package com.shapravskyi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sugar",
        "fructose",
        "vanilla",
        "water",
        "milk"
})
public class Ingredient {

    private int sugar;
    private int fructose;
    private int vanilla;
    private int water;
    private int milk;

    /**
     * No args constructor for use in serialization
     *
     */
    public Ingredient() {
    }

    /**
     *
     * @param fructose
     * @param vanilla
     * @param sugar
     */
    public Ingredient(int sugar, int fructose, int vanilla, int water, int milk) {
        this.sugar = sugar;
        this.fructose = fructose;
        this.vanilla = vanilla;
        this.water = water;
        this.milk = milk;
    }

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public int getFructose() {
        return fructose;
    }

    public void setFructose(int fructose) {
        this.fructose = fructose;
    }

    public int getVanilla() {
        return vanilla;
    }

    public void setVanilla(int vanilla) {
        this.vanilla = vanilla;
    }

    @Override
    public String toString() {
        return "Sugar: " + sugar + "mg, " + "Fructose: " + fructose + "mg, " +
                "Vanilla: " + vanilla + "mg, " + "Water: " + water + "ml, " + "Milk: " + milk;
    }
}
