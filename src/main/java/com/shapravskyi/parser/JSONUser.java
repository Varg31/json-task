package com.shapravskyi.parser;

import java.io.File;
import java.util.List;
import java.util.Map;

public class JSONUser {

    public static void main(String[] args) {
        File json = new File("src/main/resources/candyJSON.json");
        File schema = new File("src/main/resources/candyJSONScheme.json");

        JSONParser parser = new JSONParser();

        /**
         * Root element as Map
         */
        Map<String, Object> result = parser.getRootList(json);
        for (Map.Entry<String, Object> root: result.entrySet()) {
            System.out.println(root.getKey() + ", " + root.getValue().toString());
        }
        System.out.println();

        /**
         * Data as List<Object>
         */
        printList(parser.getCandyList(json));
    }

    private static void printList(List<Object> sweets) {
        System.out.println("JSON");
        for (Object candy : sweets) {
            System.out.println(candy.toString());
        }
    }
}