package com.shapravskyi.parser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shapravskyi.model.SweetsRoot;

import java.io.*;
import java.util.*;


public class JSONParser {
    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
        //this.objectMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
    }

    public List<Object> getCandyList(File jsonFile){
        SweetsRoot sweets = null;

        try {
            sweets = objectMapper.readValue(new FileInputStream(jsonFile), SweetsRoot.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sweets.getSweets();
    }

    public Map<String, Object> getRootList(File jsonFile) {
        Map<String, Object> jsonMap = null;
        try {
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream input = new FileInputStream(jsonFile);

        jsonMap = objectMapper.readValue(input,
                new TypeReference<Map<String,Object>>(){});

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonMap;
    }
}